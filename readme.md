A Tetris clone with Zig + raylib. The original Tetris is better than this.

![screenshot](docs/scshot0.webp)

This is an exercise for me to write good code in Zig. To understand the program, start by reading `src/main.zig`.  
For best reading experience, install [zls](https://github.com/zigtools/zls/). zls gives you "click to jump to definiton", which is very helpful for navigating through code.
