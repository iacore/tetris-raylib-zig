//! Tetris is a game played on a finite rectangular board made of squares. You know the rules already.
//!
//! ## Terminology
//! dropping/active piece
//!     the only piece that's moving.
//! held piece
//!     the piece that's saved in the HOLD slot
//! (to) freeze/fix
//!     when the dropping piece is made stationary forever

test "TableOfContents" {
    // program entry point
    _ = main;
    // constants
    _ = .{TILE_SIZE};
    // game logic
    _ = GameState;
    // data structures (this section is not well documented, sorry!)
    _ = .{
        TetrisShape,
        Tetris,
        TilePos,
        TileState,
        GameGrid,
        TetrisShapeRng,
    };
    // animation (unused)
    _ = anim;
}

const std = @import("std");
const rl = @import("raylib");

pub fn main() anyerror!void {
    var gpa = std.heap.GeneralPurposeAllocator(.{}){};
    var game = GameState.init(gpa.allocator());

    rl.initWindow(INFOPANEL_X + INFOPANEL_WIDTH, INFOPANEL_HEIGHT, "Tetris");
    defer rl.closeWindow();
    rl.setTargetFPS(60);

    game.run_loop();
}

//> constants for drawing
const TILE_SIZE = 24;
const GAP_SIZE = 4;
const NTILEY = 20;
const NTILEX = 10;
const INFOPANEL_X = NTILEX * (GAP_SIZE + TILE_SIZE) - GAP_SIZE;
const INFOPANEL_WIDTH = 220;
const INFOPANEL_HEIGHT = NTILEY * (GAP_SIZE + TILE_SIZE) - GAP_SIZE;

const K = rl.KeyboardKey;
/// input map for gameplay
/// is comptime heterogeneous tuple
/// tuple fields are actions (precisely what to do when the key is pressed)
const INPUT_SCHEME = .{
    .{ .key = K.key_space, .drop = 1, .reset_timer = 1 },
    .{ .key = K.key_t, .move = TilePos{ -1, 0 } },
    .{ .key = K.key_s, .move = TilePos{ 1, 0 } },
    .{ .key = K.key_n, .move = TilePos{ 0, 1 }, .move_failed_freeze = 1, .reset_timer = 1 },
    .{ .key = K.key_r, .hold = 1, .reset_timer = 1 },
    .{ .key = K.key_c, .rotate = .ccw },
    .{ .key = K.key_l, .rotate = .cw },
};
const KEY_RESET = K.key_r;

const GameState = struct {
    //> gameplay elements (active in play)
    /// the game board; contains the state of squares
    grid: GameGrid = .{},
    /// the current dropping piece; not fixed yet
    current_piece: Tetris,

    /// piece in HOLD
    held_piece: ?Tetris = null,
    /// opposite of "if the player can use HOLD ability now"
    hold_used: bool = false,
    /// score
    total_lines_cleared: u16 = 0,
    /// is game over?
    lost: bool = false,

    //> hidden state
    /// decides which Tetris piece to spawn next
    rtg: TetrisShapeRng,
    /// on every tick, the piece moves down a bit
    tick_timer: std.time.Timer,

    /// animation. currently unused
    animations: anim.AnimationList,

    const Self = @This();

    /// Reset game state
    /// used when restarting the game
    fn reset(self: *Self) void {
        self.grid = .{};
        self.spawn_new_piece() catch unreachable;
        self.tick_timer.reset();
        self.animations.clear_all();
        self.total_lines_cleared = 0;
        self.lost = false;
    }

    //> boilerplate
    fn init(alloc: std.mem.Allocator) Self {
        const now = std.time.milliTimestamp();
        var r = Self{
            .current_piece = undefined, // will init in reinit()
            .animations = anim.AnimationList.init(alloc, rl.getTime()),
            .rtg = TetrisShapeRng.init(@as(u64, @bitCast(now))),
            .tick_timer = std.time.Timer.start() catch |err| std.debug.panic("No Timer {}", .{err}),
        };
        r.reset();
        return r;
    }
    fn deinit(self: Self) void {
        self.animations.deinit();
    }

    fn end_game(self: *Self) void {
        std.log.info("Game Over", .{});
        self.lost = true;
    }

    fn run_loop(self: *Self) void {
        self.draw(); // because the game only draws when something moves, a first draw is issued here
        while (!rl.windowShouldClose()) {
            rl.pollInputEvents();
            self.update_and_draw();
        }
    }

    var text_buffer = [1]u8{0} ** 32;

    /// draws everything. the only code that put stuff on screen
    fn draw(self: Self) void {
        rl.beginDrawing();
        defer rl.endDrawing();
        rl.clearBackground(rl.Color.ray_white);
        self.grid.draw();

        rl.drawText("HOLD", INFOPANEL_X + 4, 0 + 4, 20, if (self.hold_used) rl.Color.gray else rl.Color.black);
        const str_cleared = std.fmt.bufPrintZ(&text_buffer, "Cleared: {}", .{self.total_lines_cleared}) catch unreachable;

        rl.drawText(str_cleared, INFOPANEL_X + 4, 0 + 4 + 20 + 4, 20, rl.Color.black);

        if (self.held_piece) |piece| {
            rl.drawText("HELD:", INFOPANEL_X + 4, 0 + 4 + 40 + 4 + 4, 20, rl.Color.black);
            for (piece.tiles()) |tile| {
                draw_tile(.fixed, tile, .{ INFOPANEL_X - 20, 0 + 4 + 80 + 4 });
            }
        }

        if (self.lost) {
            rl.drawText("Game Over\nPress [R] to restart", 0, 0, 20, rl.Color.black);
        }
    }

    /// - check inputs
    /// - if redraw needed, redraw
    fn update_and_draw(self: *Self) void {
        self.animations.update_clock(rl.getTime());

        const needed_redraw = self.process_input_if_needed_redraw() catch |err| switch (err) {
            error.GameOver => r: {
                self.end_game();
                break :r true;
            },
        };

        if (needed_redraw) self.draw();
    }

    /// update game state based on player input
    /// if the game ended on this frame, returns error.GameOver
    /// otherwise, returns _if redraw is needed_
    fn process_input_if_needed_redraw(self: *Self) error{GameOver}!bool {
        var dirty = false;
        if (self.lost) {
            if (rl.isKeyPressed(KEY_RESET)) {
                self.reset();
                return true;
            }
            return false;
        }

        inline for (INPUT_SCHEME) |ACTION| {
            if (rl.isKeyPressed(ACTION.key)) {
                if (@hasField(@TypeOf(ACTION), "reset_timer")) {
                    self.tick_timer.reset();
                }
                if (@hasField(@TypeOf(ACTION), "drop")) {
                    try self.drop_current();
                }
                if (@hasField(@TypeOf(ACTION), "move")) {
                    if (self.move_piece(ACTION.move) == .movement_blocked) {
                        if (@hasField(@TypeOf(ACTION), "move_failed_freeze")) {
                            try self.freeze_current();
                        }
                    }
                }
                if (@hasField(@TypeOf(ACTION), "hold")) {
                    try self.hold_piece();
                }
                if (@hasField(@TypeOf(ACTION), "rotate")) {
                    self.grid.clear(self.current_piece) catch unreachable;
                    self.current_piece.try_spin(self.grid, ACTION.rotate);
                    self.grid.set(self.current_piece) catch unreachable;
                }
                dirty = true;
            }
        }

        // auto progress
        if (self.tick_timer.read() > 1 * std.time.ns_per_s) {
            self.tick_timer.reset();
            try self.tick();
            dirty = true;
        }
        return dirty;
    }

    // ACTIONS

    /// The DROP action
    /// drop a piece as low as possible, then freeze it
    fn drop_current(self: *Self) error{GameOver}!void {
        self.grid.clear(self.current_piece) catch unreachable;
        var moved: Tetris = self.current_piece;
        while (true) {
            if (self.grid.can_fit(moved.moved(.{ 0, 1 }))) {
                moved = moved.moved(.{ 0, 1 });
            } else {
                break;
            }
        }
        self.grid.set(moved) catch return error.GameOver;
        self.current_piece = moved;
        try self.freeze_current();
    }

    /// The HOLD action
    /// hold current piece, and use previously held piece
    fn hold_piece(self: *Self) !void {
        if (self.hold_used) return;

        self.grid.clear(self.current_piece) catch unreachable;
        self.current_piece.reset_offset();
        if (self.held_piece) |*held_piece| {
            std.mem.swap(Tetris, held_piece, &self.current_piece);
            self.grid.set(self.current_piece) catch unreachable;
        } else {
            self.held_piece = self.current_piece;
            try self.spawn_new_piece();
        }
        self.hold_used = true;
    }

    /// Result of an attempt to move the dropping piece
    /// auxillary. Not a boolean because boolean is confusing.
    const MoveResult = enum {
        // successfully moved
        success,
        movement_blocked,
    };

    /// The MOVE action
    /// move the dropping piece by one square in valid direction
    fn move_piece(self: *Self, by: TilePos) MoveResult {
        const moved = self.current_piece.moved(by);
        if (self.grid.can_fit(moved)) {
            self.grid.clear(self.current_piece) catch unreachable;
            self.grid.set(moved) catch unreachable;
            self.current_piece = moved;
            return .success;
        } else {
            return .movement_blocked;
        }
    }

    /// ACTION UTILITY
    fn spawn_new_piece(self: *Self) error{GameOver}!void {
        self.hold_used = false;
        self.current_piece = self.rtg.next_random();
        self.grid.set(self.current_piece) catch return error.GameOver;
    }

    /// fix the moving piece in place forever
    fn freeze_current(self: *Self) error{GameOver}!void {
        self.grid.freeze(self.current_piece) catch return error.GameOver;

        const lines_cleared = self.grid.clear_completed();
        self.total_lines_cleared += lines_cleared;
        if (lines_cleared > 0) std.log.debug("clear {} lines", .{lines_cleared});
        // todo: maybe record combo here? (consecutive pieces dropped, each clearing some line)

        try self.spawn_new_piece();
    }

    /// when time moves forward enough, the dropping piece is moved down by 1
    fn tick(self: *Self) error{GameOver}!void {
        if (self.move_piece(.{ 0, 1 }) == .movement_blocked) {
            try self.freeze_current();
        }
    }
};

// == data structures section start ==

/// defines where rotation anchor is, and how many rotations a piece has
const SpinBehavior = union(enum) {
    none,
    t, // t-spin
    half_cw, // only two rotations, 0 and 90deg cw
    half_ccw, // only two rotations, 0 and 90deg ccw
    full, // the normal 4-state rotation
};

/// rotation anchor is .{0,0}
const TetrisShape = struct {
    tiles: [4]TilePos,
    /// can be rotated if true
    spin_like: SpinBehavior,

    fn default_offset(this: @This()) TilePos {
        _ = this;
        return .{ @divTrunc(NTILEX, 2), -1 };
    }
};
const ALL_SHAPES = [_]TetrisShape{
    // square
    .{
        .tiles = .{ .{ 0, 0 }, .{ 0, 1 }, .{ -1, 0 }, .{ -1, 1 } },
        .spin_like = .none,
    },
    // T
    .{
        .tiles = .{ .{ 0, 0 }, .{ -1, 0 }, .{ 1, 0 }, .{ 0, -1 } },
        .spin_like = .t,
    },
    // L
    .{
        .tiles = .{ .{ 0, 0 }, .{ 0, 1 }, .{ 0, -1 }, .{ -1, -1 } },
        .spin_like = .full,
    },
    // flipped L
    .{
        .tiles = .{ .{ 0, 0 }, .{ 0, 1 }, .{ 0, -1 }, .{ 1, -1 } },
        .spin_like = .full,
    },
    // line
    .{
        .tiles = .{ .{ 0, 0 }, .{ 0, 1 }, .{ 0, -1 }, .{ 0, -2 } },
        .spin_like = .half_cw,
    },
    // S
    .{
        .tiles = .{ .{ 0, 0 }, .{ -1, 0 }, .{ 0, 1 }, .{ 1, 1 } },
        .spin_like = .half_ccw,
    },
    // Z
    .{
        .tiles = .{ .{ 0, 0 }, .{ 1, 0 }, .{ 0, 1 }, .{ -1, 1 } },
        .spin_like = .half_cw,
    },
};

/// which direction is SPIN action going to spin a piece
const SpinDirection = enum {
    ccw, // ccw 90deg
    cw, // cw 90deg
};

const Tetris = struct {
    shape: *const TetrisShape,
    offset: TilePos,
    /// 0 = no spin, 1 = 90deg cw, etc
    spin: u2 = 0,

    const Self = @This();

    fn reset_offset(self: *Self) void {
        self.offset = self.shape.default_offset();
    }

    fn tiles(self: Self) [4]TilePos {
        var r = self.shape.tiles;
        for (&r) |*tile_ptr| {
            const tile = tile_ptr.*;
            tile_ptr.* = switch (self.spin) {
                0 => tile,
                1 => .{ -tile[1], tile[0] },
                2 => -tile,
                3 => .{ tile[1], -tile[0] },
            };
            tile_ptr.* += self.offset;
        }
        return r;
    }

    fn moved(self: Self, delta: TilePos) Self {
        var r = self;
        r.offset += delta;
        return r;
    }

    // todo: make spin right
    // the authentic Tetris® has strict rules about this. among them is "t-spin"
    fn try_spin(self: *Self, grid_without_self: GameGrid, comptime rotate_dir: SpinDirection) void {

        // if (rotate_dir != 1 and rotate_dir != -1) @compileError(std.fmt.comptimePrint("Invalid rotate_dir: {}", .{rotate_dir}));
        const spin0 = self.spin;
        // const spin_ = @intCast(i8, span0) + rotate_dir;
        const spin_profile: [4]u2 = switch (self.shape.spin_like) {
            .none => .{ 0, 0, 0, 0 },
            .half_cw => .{ 1, 0, 1, 0 },
            .half_ccw => .{ 3, 0, 3, 0 },
            .full => .{ 1, 2, 3, 0 },
            .t => .{ 1, 2, 3, 0 },
        };

        self.spin = switch (rotate_dir) {
            .cw => spin_profile[spin0],
            .ccw => spin_profile[spin_profile[spin_profile[spin0]]],
        };

        // if can't fit, revert the spin
        if (!grid_without_self.can_fit(self.*)) {
            self.spin = spin0;
        }
    }
};

const TilePos = @Vector(2, i8);

const TileState = enum {
    empty,
    fixed,
    moving,

    fn getColor(tile: @This()) rl.Color {
        return switch (tile) {
            .empty => rl.Color.light_gray,
            .fixed => rl.Color.black,
            .moving => rl.Color.orange,
        };
    }
};

pub fn draw_tile(tile: TileState, pos: TilePos, offset: @Vector(2, c_int)) void {
    rl.drawRectangle(@as(c_int, @intCast(pos[0])) * (TILE_SIZE + GAP_SIZE) + offset[0], @as(c_int, @intCast(pos[1])) * (TILE_SIZE + GAP_SIZE) + offset[1], TILE_SIZE, TILE_SIZE, tile.getColor());
}

const GameGrid = struct {
    tiles: [NTILEY][NTILEX]TileState = .{.{.empty} ** NTILEX} ** NTILEY,

    const Self = @This();
    pub fn draw(self: Self) void {
        // todo: draw shadow/drop indicator (where the block would be if it was dropped)

        for (self.tiles, 0..) |row, y| {
            for (row, 0..) |tile, x| {
                draw_tile(tile, TilePos{ @as(i8, @intCast(x)), @as(i8, @intCast(y)) }, .{ 0, 0 });
            }
        }
    }

    fn set_from_to(self: *Self, piece: Tetris, from: ?TileState, to: ?TileState) !void {
        for (piece.tiles()) |tile| {
            // bounds checking
            if (!(0 <= tile[1] and tile[1] < NTILEY and
                0 <= tile[0] and tile[0] < NTILEX)) continue;

            const x = @as(usize, @intCast(tile[0]));
            const y = @as(usize, @intCast(tile[1]));

            const ref_tile = &self.tiles[y][x];
            // std.log.debug("{?} {?} {?}", .{tile[0], tile[1], ref_tile});
            if (from) |from0| {
                if (ref_tile.* != from0) {
                    std.debug.panic("impossible: {},{} {} has:{}", .{ x, y, from0, ref_tile.* });
                    continue;
                }
            }
            if (to) |to0| ref_tile.* = to0;
        }
    }

    pub fn can_fit(self: Self, piece: Tetris) bool {
        for (piece.tiles()) |tile| {
            // bounds checking
            if (!(tile[1] < NTILEY and
                0 <= tile[0] and tile[0] < NTILEX)) return false;
            if (!(0 <= tile[1])) continue; // if above screen, skip this tile

            const x = @as(usize, @intCast(tile[0]));
            const y = @as(usize, @intCast(tile[1]));

            if (0 <= tile[1]) {
                const tilestate = self.tiles[y][x];
                if (tilestate == .fixed) return false;
            }
        }
        return true;
    }

    pub fn clear(self: *Self, piece: Tetris) !void {
        try self.set_from_to(piece, .moving, .empty);
    }

    pub fn set(self: *Self, piece: Tetris) !void {
        try self.set_from_to(piece, .empty, .moving);
    }

    pub fn freeze(self: *Self, piece: Tetris) error{ GameOver, CantFit }!void {
        for (piece.tiles()) |tile| {
            if (tile[1] < 0) return error.GameOver;
        }
        try self.set_from_to(piece, .moving, .fixed);
    }

    fn is_line_completed(self: *Self, i: usize) bool {
        for (self.tiles[i]) |tile| {
            if (tile != .fixed) return false;
        } else {
            return true;
        }
    }

    /// @return  number of lines cleared
    pub fn clear_completed(self: *Self) u16 {
        // todo: add animation
        var lines_cleared: u16 = 0;
        var i: isize = self.tiles.len - 1;
        while (i >= 0) {
            if (self.is_line_completed(@as(usize, @intCast(i)))) {
                lines_cleared += 1;
                var j = i - 1;
                while (j >= 0) : (j -= 1) {
                    self.tiles[@as(usize, @intCast(j + 1))] = self.tiles[@as(usize, @intCast(j))];
                }
            } else {
                i -= 1;
            }
        }
        return lines_cleared;
    }
};

/// Picks shape of the next piece to spawn
///
/// In games adhering to the official Tetris Guidelines, most notably TDS, the randomizer is known to not be purely random.
/// In fact, the randomizer's rules are quite simple:
/// Give the player each of the 7 tetrominoes, in a random order.
/// Give the player each of the 7 tetrominoes, in a random order.
/// Give the player each of the 7 tetrominoes, in a random order.
///
/// This implements that algorithm.
pub const TetrisShapeRng = struct {
    const Prng = std.rand.DefaultPrng;

    prng: Prng,
    seen: [ALL_SHAPES.len]bool,
    num_left: usize,

    fn reset(this: *@This()) void {
        this.seen = .{false} ** ALL_SHAPES.len;
        this.num_left = ALL_SHAPES.len;
    }

    pub fn init(seed: u64) @This() {
        var r = @This(){
            .prng = Prng.init(seed),
            .seen = undefined,
            .num_left = undefined,
        };
        r.reset();
        return r;
    }
    pub fn next_random(this: *@This()) Tetris {
        if (this.num_left == 0) this.reset();
        const nth_free_asked = this.prng.random().uintLessThan(usize, this.num_left);
        this.num_left -= 1;

        var nth_free: usize = 0;
        var i: usize = 0;

        const selected_i = sel: while (i < ALL_SHAPES.len) {
            if (this.seen[i]) {
                i += 1;
                continue;
            }
            if (nth_free == nth_free_asked) {
                this.seen[i] = true;
                break :sel i;
            }
            i += 1;
            nth_free += 1;
        } else unreachable;

        const shape = &ALL_SHAPES[selected_i];
        return .{
            .shape = shape,

            .offset = shape.default_offset(),
        };
    }
};

// == data structures section end ==

const anim = struct {
    // const Loop = std.event.Loop;
    // const Timer = std.time.Timer;

    /// absolute time in seconds
    const Timestamp = f64;

    // /// animation hooks:
    // /// - paint atop fg
    // /// - paint between bg & fg
    // pub const IAnimDetail = interface.Interface(struct {
    //     atop_fg: ?fn(*interface.SelfType) void,
    //     atop_bg: ?fn(*interface.SelfType) void,
    // }, interface.Storage.Inline(128));

    pub const Animation = struct {
        stop_at: Timestamp,
        // detail: IAnimDetail,

        fn compareFn(_: void, a: Animation, b: Animation) std.math.Order {
            return std.math.order(a.stop_at, b.stop_at);
        }
    };

    /// All animations in list are effective
    pub const AnimationList = struct {
        queue: std.PriorityQueue(Animation, void, Animation.compareFn),
        clock: Timestamp, // current time

        const Self = @This();
        pub fn init(allocator: std.mem.Allocator, time_now: Timestamp) Self {
            var r: Self = undefined;
            r.queue = @TypeOf(r.queue).init(allocator, {});
            r.update_clock(time_now);
            return r;
        }

        pub fn deinit(self: Self) void {
            self.queue.deinit();
        }

        pub fn clear_all(self: *Self) void {
            self.queue.deinit();
            self.queue = @TypeOf(self.queue).init(self.queue.allocator, {});
        }

        // /// add animation to list
        // ///   anim: something like struct{ stop_at: Timestamp, detail: anytype }
        // pub fn add(self: *Self, anim: anytype) !void {
        //     try self.queue.add(.{
        //         .stop_at = anim.stop_at,
        //         .detail = IAnimDetail.init(anim.detail),
        //     });
        // }

        pub fn update_clock(self: *Self, time_now: Timestamp) void {
            self.clock = time_now;
            // prune tasks out of date
            while (true) {
                if (self.queue.peek()) |head| {
                    if (self.clock >= head.stop_at) {
                        _ = self.queue.remove();
                    } else break;
                } else break;
            }
        }
    };
};
